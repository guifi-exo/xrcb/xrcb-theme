<?php
/**
	Template Name: Radios GeoJSON
*/

header('Content-Type: application/json; charset=utf-8');
//header('Content-Disposition: attachment; filename="export.csv"');
$fp = fopen('php://output', 'w');

$my_query = new WP_Query('post_type=radio&post_status=publish&posts_per_page=100&order=ASC&orderby=ID');

if ( $my_query->have_posts() ) {

	$features = array(
		'features' => array(),
		'type' => 'FeatureCollection'
	);

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$location = get_post_meta(get_the_ID(), 'location', true);

		if (!empty($location) && get_post_meta(get_the_ID(), 'sede', true) !== "fantasma") {

			$features['features'][] = array(
				'id' => (int)get_the_ID(),
				'properties' => array(
					'id' => (int)get_the_ID(),
					"title" => get_the_title(),
					"categoria" => get_the_terms( get_the_ID(), 'radio_category' )[0]->name,
					"barrio" => get_post_meta(get_the_ID(), 'barrio', true),
					"address" => $location["address"],
					"web" => get_post_meta(get_the_ID(), 'web', true),
					"permalink" => get_permalink(get_the_ID()),
				),
				'type' => 'Feature',
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						(float)$location["lng"],
						(float)$location["lat"]
					)
				)
			);
		}
	}

	echo json_encode($features);
}

fclose($fp);

?>