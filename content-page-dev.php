<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package xrcb
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'xrcb' ), 'after' => '</div>' ) ); ?>

		<p>Rolling release: commit 
			<?php
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN:'.GITLAB_ACCESS_TOKEN)); 
			curl_setopt($ch, CURLOPT_URL, "https://gitlab.com/api/v4/projects/6722810/repository/commits");

			$last_commit = json_decode(curl_exec($ch))[0];
			echo "<a title='".$last_commit->title."' href='https://gitlab.com/guifi-exo/xrcb/xrcb-theme/-/commit/".$last_commit->id."'>".$last_commit->short_id."</a>";

			curl_close($ch);
			?>
		</p>

	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'xrcb' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
