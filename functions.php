<?php
/**
 * xrcb functions and definitions
 *
 * @package xrcb
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'xrcb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function xrcb_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	//require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 */
	load_theme_textdomain( 'xrcb', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'xrcb' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // xrcb_setup
add_action( 'after_setup_theme', 'xrcb_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function xrcb_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'xrcb_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'xrcb_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function xrcb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'xrcb' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'xrcb_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function xrcb_scripts() {
	/* load scripts for jquery tabs */
	if ( !is_admin() ) {
		wp_register_style( 'tabs_css', get_template_directory_uri().'/vendor/jquery-ui.min.css' );
		wp_enqueue_style( 'tabs_css' );
		wp_enqueue_script('jquery-ui-tabs');
	}

	wp_enqueue_style( 'xrcb-style', get_stylesheet_uri() );

	wp_enqueue_script( 'xrcb-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'xrcb-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'xrcb-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	/* ajaxifying theme */
	if ( ! is_admin() ) {

		/* include jquery $.browser code to work in WP 5.6 */
		wp_enqueue_script( 'browser', get_template_directory_uri() . '/js/browser.js', array(), '20201012', true );

		$url = get_stylesheet_directory_uri() . '/vendor/';
		wp_enqueue_script( 'hash-change', "{$url}jquery.ba-hashchange.min.js", array('jquery'), '', true);
		$url = get_stylesheet_directory_uri() . '/js/';
		wp_enqueue_script( 'ajax-theme', "{$url}ajax.js", array( 'hash-change' ), '', true);

		/* load mediaelements scripts and styles */
		wp_enqueue_style( 'wp-mediaelement' );
	    wp_enqueue_script( 'wp-mediaelement' );
	}

	//wp_enqueue_style( 'xrcb-custom-style', get_template_directory_uri() . '/css/custom.css' );

	// time stamp for caching
	// https://developer.wordpress.org/reference/functions/wp_enqueue_style/#comment-2056
  wp_enqueue_style( 'xrcb-custom-style', get_template_directory_uri() . '/css/custom.css', array(), filemtime(get_template_directory() . '/css/custom.css'), false);
}
add_action( 'wp_enqueue_scripts', 'xrcb_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

/* redirect after login */
function admin_default_page($redirect_to, $request, $user) {
	// forward to home, but when user tries to access publish podcast page
	if (strpos($redirect_to, 'publish-podcast') !== false) return ( $redirect_to );
  	else return ( get_site_url() );
}
add_filter('login_redirect', 'admin_default_page', 10, 3);

// Redirect to custom post type itself after publishing or updating content
// https://gist.github.com/davejamesmiller/1966595
add_filter('redirect_post_location', function($location)
{
    global $post;
    if (
        $post->post_type != 'post' &&
        $post->post_type != 'page' &&
        (isset($_POST['publish']) || isset($_POST['save'])) &&
        preg_match("/post=([0-9]*)/", $location, $match) &&
        $post &&
        $post->ID == $match[1] &&
        (isset($_POST['publish']) || $post->post_status == 'publish') &&
        $pl = get_permalink($post->ID) ) {

        // Always redirect to the post
        $location = $pl;
    }
    return $location;
});

/* Add custom logo */
function custom_login_logo() {
	echo '<style type="text/css">
	#login h1 a { background-image: url('.get_home_url().'/wp-content/uploads/2018/04/xrcb.png); background-size:230px 144px; width:230px; height:144px; }
	</style>';
}
add_action('login_head', 'custom_login_logo');

/* add custom login message */
function xrcb_login_message( $message ) {
	return '<p>Por cualquier problema con el login/registrar, <a href="mailto:info@xrcb.cat">contactanos por mail</a>.</p>';
}
add_filter('login_message', 'xrcb_login_message');

/* remove admin bar */
add_filter( 'show_admin_bar', '__return_false' );

/* hide admin menu */
function hide_menus() {
    if ( !current_user_can('manage_options') ) {

    	// hide Media menu
    	remove_menu_page( 'upload.php' );

        /*?>
        <style>
           #adminmenuback, #adminmenuwrap, #wp-admin-bar-wp-logo{
                display:none;
            }
        </style>
        <?php*/
    }
}
add_action('admin_head', 'hide_menus');

/* hide media library tab in media manager */
function remove_medialibrary_tab($tabs) {
    if ( !current_user_can( 'administrator' ) ) {
        unset($tabs["mediaLibraryTitle"]);
    }
    return $tabs;
}
add_filter('media_view_strings','remove_medialibrary_tab');

// single post category template
// https://nathanrice.me/blog/wordpress-single-post-templates/
add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-{$cat->slug}.php"; } return $t;' ));

/* add admin CSS style */
function xrcb_admin_theme_style() {
    wp_enqueue_style('xrcb', get_stylesheet_directory_uri().'/css/wp-admin.css');
    wp_enqueue_script('xrcb', get_stylesheet_directory_uri().'/js/wp-admin.js');
}
add_action('admin_enqueue_scripts', 'xrcb_admin_theme_style');
add_action('login_enqueue_scripts', 'xrcb_admin_theme_style');

/* reorder meta boxes */
function xrcb_one_column_for_all_radio( $order ) {
    return array(
        'normal'   => join( ",", array(
            'tagsdiv-post_tag',
            'postimagediv',
            'submitdiv',
        ) ),
        'side'     => '',
        'advanced' => '',
    );
}
add_filter( 'get_user_option_meta-box-order_radio', 'xrcb_one_column_for_all_radio' );

function xrcb_one_column_for_all_podcast( $order ) {
    return array(
        'normal'   => join( ",", array(
            'tagsdiv-post_tag',
            'postimagediv',
            'submitdiv',
        ) ),
        'side'     => '',
        'advanced' => '',
    );
}
add_filter( 'get_user_option_meta-box-order_podcast', 'xrcb_one_column_for_all_podcast' );

/* hide screen options */
function xrcb_remove_screen_options_tab() {
    return current_user_can( 'manage_options' );
}
add_filter('screen_options_show_screen', 'xrcb_remove_screen_options_tab');

/* set layout to one column */
function xrcb_screen_layout_columns( $columns ) {
    $columns['post'] = 1;
    return $columns;
}
add_filter( 'screen_layout_columns', 'xrcb_screen_layout_columns' );
function xrcb_screen_layout_radio() {
    return 1;
}
add_filter( 'get_user_option_screen_layout_radio', 'xrcb_screen_layout_radio' );
function xrcb_screen_layout_podcast() {
    return 1;
}
add_filter( 'get_user_option_screen_layout_podcast', 'xrcb_screen_layout_podcast' );

/* hide visual editor for all contributors */
/*function xrcb_disable_visual_editor( $can ) {
	global $post;
	if ( !current_user_can( 'manage_options' ))
		return false;
	return $can;
}
add_filter( 'user_can_richedit', ' xrcb_disable_visual_editor ' );*/

/* hide add media button */
function xrcb_remove_addMediaButtons(){
    if ( !current_user_can( 'manage_options' ) ) {
        remove_action( 'media_buttons', 'media_buttons' );
    }
}
add_action('admin_head', 'xrcb_remove_addMediaButtons');

/* filter radio field in podcasts to only show own radio */
/*function xrcb_post_object_query( $args, $field, $post ) {
	$args['author_id'] = $post->post_author;
	echo '<pre>';
	print_r($field);
	print_r($args);
	echo '</pre>';
	//die;
    return $args;
}
add_filter('acf/fields/post_object/query/key=field_5b2a14e0b2380', 'xrcb_post_object_query', 10, 3);*/


/* http://werdswords.com/force-sub-categories-use-the-parent-category-template/ */
function xrcb_new_subcategory_hierarchy() {
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php';
    }
    return locate_template( $templates );
}

add_filter( 'category_template', 'xrcb_new_subcategory_hierarchy' );

// wp-postratings
// commented on 2022 12 13 as there is no png image in server, just gif
//
//function xrcb_rating_image_extension() {
//    return 'png';
//}
//add_filter( 'wp_postratings_image_extension', 'xrcb_rating_image_extension' );

// extend appointment if bigger than slot time (>30min)
function xrcb_create_appointment( $appointment_id, $appointment_data ) {

	//error_log($appointment_data["duracion"]);
	//trigger_error(json_encode($appointment_data), E_USER_WARNING);

	global $wpdb;
	$length = 1800;	// default=30min

	// get podcast id
	$podcastId = ($wpdb->get_row("SELECT value from wp_ea_fields WHERE app_id=".$appointment_id." AND field_id=5"))->value;

	if ($podcastId) {
		// 1. PODCAST booking
		
		$live = get_post_meta($podcastId, "live", true);

		$length = 0;
		if (empty($live)) {
			// has podcast attached
			$length = wp_get_attachment_metadata(get_post_meta($podcastId, "file_mp3", true))["length"];
		}
		else {
			// has no podcast attached -> live broadcasting
			$length = get_post_meta($podcastId, "live_duration", true);
		}
	}
	else {
		// 2. LIVE booking

		// get event duration
		$duracion = ($wpdb->get_row("SELECT value from wp_ea_fields WHERE app_id=".$appointment_id." AND field_id=12"))->value;

		if ($duracion) {
			$length = intval($duracion)*60;
		}
	}

	// check if podcast lasts longer than slot of 30 minutes
	if ($length > 1800) {

		// change end time of podcast
		$slots = ceil($length/1800);
		$endtime = strtotime($appointment_data["start"]) + $slots*30*60;
		$endtimeStr = date('H:i:s', $endtime);
		//error_log("podcastId:".$podcastId." (length:".$length."sec=".$slots." slots), starttime:".$appointment_data["start"]." endtime:".$appointment_data["end"]." - NEW endtime:".$endtimeStr);

		// check if new endtime interferers with following event
		$interfereEvent = $wpdb->get_results("SELECT * from wp_ea_appointments WHERE date='".$appointment_data["date"]."' AND end_date='".$appointment_data["end_date"]."' AND start < CAST('".$endtimeStr."' AS time) AND end > CAST('".$endtimeStr."' AS time)");
		if (count($interfereEvent) < 1) {

			//UPDATE wp_ea_appointments SET end = CAST('11:30:00' AS TIME) WHERE id='74';
			$wpdb->update(
				"wp_ea_appointments",
				array("end" => $endtimeStr),
				array("id" => $appointment_id)
			);
		}
	}
}
add_action( 'ea_new_app_from_customer', 'xrcb_create_appointment', 10, 2 );

// https://support.advancedcustomfields.com/forums/topic/dynamically-set-acf-field-default-value/
$member_id = get_current_user_id();

add_filter('acf/load_field/name=usuari',
     function($field) use ($member_id) {
      // the variable after 'use' ($member_id) indicates that it is the one to 'use' from the main script.  $field is coming from 'acf/load_field'.
     $field['default_value'] = $member_id;
     return $field;
 }
);

// trigger notification when new podcast is created
function xrcb_notify_new_podcast( $new_status, $old_status, $post ) {
  if (get_post_type($post) !== 'podcast')
        return;    //Don't touch anything that's not a podcast

    //error_log(print_r(get_post_meta($post->ID, 'live', true), true));

    // trigger notification only when first time publish and not for live broadcasting podcasts
    if( 'publish' !== $old_status &&
    	$new_status === 'publish' &&
    	get_post_meta($post->ID, 'live', true) == "false") {

    	$msg = 'Nou podcast de '.get_the_author_meta("display_name", $post->post_author).': "'.$post->post_title.'" '.get_home_url().'/ca/podcast/'.$post->post_name;
    	//error_log($msg);
		sendCurlNotification($msg);
    }
}
add_action('transition_post_status', 'xrcb_notify_new_podcast', 10, 3 );

function sendCurlNotification($msg) {
    // define variables in wp-config.php:
    //   MATRIX_HOMESERVER: matrix.example.com
    //   MATRIX_ROOM: !room:matrix.example.com
    //   MATRIX_ACCESS_TOKEN: *check your token access in riot general settings*
	$url = "https://" . MATRIX_HOMESERVER . "/_matrix/client/r0/rooms/" . MATRIX_ROOM . "/send/m.room.message?access_token=" . MATRIX_ACCESS_TOKEN;

	$post = array(
		"msgtype" => "m.text",
		"body" => $msg,
		"format" => "org.matrix.custom.html",
		"formatted_body" => $msg
	);

	curl_post($url, $post);
}

/**
* Send a POST requst using cURL
* @param string $url to request
* @param array $post values to send
* @param array $options for cURL
* @return string
*/
function curl_post($url, array $post = NULL, array $options = array())
{
    $defaults = array(
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 4,
		CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POSTFIELDS => json_encode($post)
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

// add custom columns to podcast backend table
// https://wordpress.stackexchange.com/questions/253680/add-custom-column-to-custom-post-type-overview-in-backend
add_filter('manage_podcast_posts_columns', 'xrcb_podcasts_columns');
function xrcb_podcasts_columns($columns) {
    $columns['duration'] = 'Duración';
    $columns['size'] = 'Tamaño';
    $columns['bitrate'] = 'Bitrate';
    return $columns;
}

add_action('manage_podcast_posts_custom_column',  'xrcb_show_podcasts_columns');
function xrcb_show_podcasts_columns($name) {
    global $post;
    $meta = wp_get_attachment_metadata(get_post_meta($post->ID, 'file_mp3', true));
    switch ($name) {
        case 'duration':
        	if (is_array($meta) && array_key_exists('length_formatted', $meta)) {
            	echo $meta['length_formatted'];
        	}
            break;
        case 'size':
        	if (is_array($meta) && array_key_exists('filesize', $meta)) {
	        	$num = $meta['filesize']/1000000;
	            echo number_format($num, 2, ',', '.') . ' MB';
	        }
            break;
        case 'bitrate':
        	if (is_array($meta) && array_key_exists('bitrate', $meta)) {
	        	$num = $meta['bitrate']/1000;
	            echo number_format($num, 2, ',', '.') . ' kbit/s';
	        }
            break;
    }
}

/*add_filter( 'manage_edit-podcast_sortable_columns', 'xrcb_sortable_podcasts_columns' );
function xrcb_sortable_podcasts_columns( $columns ) {
    $columns['duration'] = 'Duración';
    $columns['size'] = 'Tamaño';
    $columns['bitrate'] = 'Bitrate';
    return $columns;
}

// https://bamadesigner.com/2014/04/how-to-make-your-wordpress-admin-columns-sortable/#register-sortable-columns
add_action( 'pre_get_posts', 'xrcb_wp_posts_be_qe_pre_get_posts', 1 );
function xrcb_wp_posts_be_qe_pre_get_posts( $query ) {
   if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
      switch( $orderby ) {
         case 'size':
            $query->set( 'meta_key', 'size' );
            $query->set( 'orderby', 'meta_value' );
            break;
         case 'bitrate':
            $query->set( 'meta_key', 'bitrate' );
            $query->set( 'orderby', 'meta_value_num' );
            break;
      }
   }
}*/
