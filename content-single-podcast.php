<?php
/**
 * @package xrcb
 */
?>

<?php
	function getPodcastDescription($id) {

		$desc = '';
		$txt = apply_filters( 'the_content', get_the_content($id) ); 

		if ($txt && strlen($txt) > 200) {  
			$desc .= '<div class="resume">' . rtrim(substr(strip_tags($txt),0,200)) . '...</div>';
			$desc .= '<div class="total">' . $txt . '</div>';
			$desc .= '<span class="more-button"><i class="fa fa-plus" aria-hidden="true"></i></span>';
			$desc .= '<script>
				jQuery(document).ready(function($) {
					$(".more-button").click(function() {
						$(".description .resume").toggle();
						$(".description .total").toggle();
						$(".more-button .fa").toggleClass("fa-plus");
						$(".more-button .fa").toggleClass("fa-minus");
					});
				});
			</script>';
		} else {
			$desc .= $txt;
		}

		return $desc;
	}
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php if (get_field('img_podcast')) : ?>

				<?php if (get_post_meta(get_the_ID(), 'radio', true) != 864) : ?>

					<img class="attachment-full size-full wp-post-image" src="<?php echo get_field('img_podcast')['url']; ?>" />

				<?php else : ?>

					<img class="attachment-full size-full wp-post-image" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/09/gif-01-3.gif))" />

				<?php endif; ?>

			<?php endif; ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			$programa = "";
			$programa_tmp = get_the_terms( get_the_ID(), 'podcast_programa');
			if ($programa_tmp && count($programa_tmp) == 1) {
				$programa = $programa_tmp[0]->name;
			}
		?>
		<div class="btn-play-container">
			<div class='btn btn-play btn-play-inverse piwik_download' data-src='<?php echo (get_post_meta(get_the_ID(), 'live', true) == 'true' ? 'https://icecast.xrcb.cat/main.mp3' : wp_get_attachment_url(get_post_meta(get_the_ID(), 'file_mp3', true))); ?>' data-radio='<?php echo get_the_title(get_post_meta(get_the_ID(), 'radio', true)); ?>' data-title='<?php the_title(); ?>' data-radio-link='<?php echo get_the_permalink(get_post_meta(get_the_ID(), 'radio', true)); ?>' data-podcast-link='<?php echo get_the_permalink(); ?>' data-programa='<?php echo $programa; ?>'></div>
		</div>

		</br>
		<?php 
			//if ( has_post_thumbnail() ) the_post_thumbnail('thumbnail', array('class' => 'alignright'));
		?>

		<div class="description">
			<?php echo getPodcastDescription($post->ID); ?>
		</div>
		</br>

		<ul>

			<?php
				$location = get_post_meta(get_post_meta(get_the_ID(), 'radio', true), 'location', true);
				$address = "";
				$lat = 0;
				$lon = 0;
				if ($location) {
					$address = $location["address"];
					$lat = (float)$location["lat"];
					$lon = (float)$location["lng"];
				}
			?>

			<div class="line line1">
				<span class="radio">
					<a class="btn-radio" href="<?php echo get_the_permalink(get_post_meta(get_the_ID(), 'radio', true)); ?>" data-lat="<?php echo $lat; ?>" data-lon="<?php echo $lon; ?>"><?php echo get_the_title(get_post_meta(get_the_ID(), 'radio', true)); ?></a>
					<a target="_blank" id="btn-subscribe-radio" class="btn-cat" href="/subscribirse-a-podcast/?id=<?php echo get_post_meta(get_the_ID(), 'radio', true); ?>">SUBSCRIBE TO RADIO</a>
				</span>
			</div>

			<li class="subline">
				<?php 
					if (get_field('fecha_emision')) {
						the_field('fecha_emision'); 
					}
					else {
						echo get_the_date('d/m/Y');
					}
				?> / 

				<?php if (get_the_terms( get_the_ID(), 'podcast_tag', '', ', ', '')): ?>
					<?php 
					$terms = get_the_terms( get_the_ID(), 'podcast_tag' );
	                         
					if ( $terms && ! is_wp_error( $terms ) ) : 
					    $draught_links = array();
					    foreach ( $terms as $term ) {
					        $draught_links[] = $term->name;
					    }           
					    $on_draught = join( ", ", $draught_links );
					    ?>
					 
					    <?php printf( '%s', esc_html( $on_draught ) ); ?>
					<?php endif; ?>
				<?php endif; ?>

				<!--<?php //if (get_the_term_list( get_the_ID(), 'podcast_tag', '', ', ', '')): ?>
					<li><strong><?php //_e( 'Etiquetas', 'xrcb' ); ?>: </strong><?php //echo get_the_term_list( get_the_ID(), 'podcast_tag', '', ', ', '' ); ?></li>
				<?php //endif; ?>-->
			</li>

			<br />

			<?php if (get_the_term_list( get_the_ID(), 'podcast_programa', '', ', ', '')): ?>
				<?php 
				$terms = get_the_terms( get_the_ID(), 'podcast_programa' );
                         
				if ( !is_wp_error($terms) && count($terms) == 1 ) : 
					$programa = reset($terms)->name;
				    ?>
				 
				    <li><strong><?php _e( 'Programa', 'xrcb' ); ?>: </strong>
				    	<a href="<?php echo get_the_permalink(get_post_meta(get_the_ID(), 'radio', true)).'?program='.reset($terms)->term_id; ?>"><?php printf( '%s', esc_html( $programa ) ); ?></a>
						<a target="_blank" id="btn-subscribe-program-podcast" class="btn-cat" href="/subscribirse-a-podcast/?id=<?php echo get_post_meta(get_the_ID(), 'radio', true); ?>&id_program=<?php echo reset($terms)->term_id; ?>">SUBSCRIBE TO PROGRAM</a>
				    </li>
				<?php endif; ?>
			<?php endif; ?>

			<li>
				<br />
				<?php $file = wp_get_attachment_url(get_post_meta($post->ID, 'file_mp3', true)); ?>
				<a href="<?php echo $file; ?>" download class="icon downloadlink"><i class="fa fa-15x fa-arrow-down" aria-hidden="true"></i></a> 
				<i class="fa fa-15x fa-code btn-embed" aria-hidden="true"></i> 
				<i class="fa-15x fab fa-twitter btn-twitter" aria-hidden="true"></i> 
				<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
			</li>

			<li class="embed_code">
				<?php
					$src = 'https://player.xrcb.cat/?url='.$file;
					$src .= '&podcast='.get_the_title();
					$src .= '&podcastlink='.get_permalink();
					$src .= '&radio='.get_the_title(get_post_meta(get_the_ID(), 'radio', true));
					$src .= '&radiolink='.get_the_permalink(get_post_meta(get_the_ID(), 'radio', true));
				?>

				<pre>&lt;iframe width="600" height="60" src="<?php echo $src; ?>" frameborder="0" scrolling="no" style="overflow: hidden; max-width: 100%;"&gt;Iframe no esta soportado en tu navegador.&lt;/iframe&gt;</pre>
			</li>
		</ul>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'xrcb' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'xrcb' ) );

			if ( ! xrcb_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				}

			} // end check for categories on this blog

			/*printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);*/

			if (is_user_logged_in() && get_the_author_meta('ID') == get_current_user_id()) {
				echo '<span class="edit-link"><a class="post-edit-link" href="' . get_home_url( null, 'publish-podcast/?post_id=' . get_the_ID() ) . '">Edit</a></span>';
			}
		?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		bindPlayer();

		setTimeout(loadAudioOnMobile, 500, $);

	    $("a.btn-radio").click(function(){
	        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
	    });

	    $(".podcast .btn-embed").click(function(){
	    	$(".podcast .embed_code").toggle();
	    });

	    $(".podcast .btn-twitter").click(function(){
			createTwitterMsg($(".btn-radio").text(), $(".podcast .entry-title").text(), document.location.href);
	    });

		function loadAudioOnMobile($) {
			// load podcast into player when on mobile phone
			//console.log("show", isPlaying, window.mobilecheck(), player);
			if (window.mobilecheck()) {
				// set background image
				var gif;
				<?php 
					$radio = get_post_meta(get_the_ID(), 'radio', true);
					if (has_post_thumbnail( $radio ) ) {
						echo "gif='" . get_the_post_thumbnail_url($radio) ."';";
					}
				?>

				//console.log("gif",gif);
				if (gif) {
					// show radio gif if exists
					$("#mobileplayer").css("background-image", "url("+gif+")");
				}
				else {
					// show default gif if not exists
					$("#mobileplayer").css("background-image", "url(<?php echo get_home_url(); ?>/wp-content/uploads/2018/07/gof-01_transp.gif))");
				}

			//	$("#primary .close-button").click();
			}

			if (!isPlaying) {
				// load audio in player
				player.setSrc($(".downloadlink").attr("href"));
				$(".xrcbplayer .player").removeClass("playing-off");
				$(".xrcbplayer .player").addClass("playing-on");

				var radio = "<?php echo get_the_title(get_post_meta(get_the_ID(), 'radio', true)); ?>";
				$(".xrcbplayer .radio").text(radio);
				$(".xrcbplayer .radio-link").text("<?php echo get_the_permalink(get_post_meta(get_the_ID(), 'radio', true)); ?>");
				$(".xrcbplayer .sep").css("display", "inline-block");

				<?php
					$programa = "";
					$programa_tmp = get_the_terms( get_the_ID(), 'podcast_programa');
					if ($programa_tmp && count($programa_tmp) == 1) {
						$programa = "[".$programa_tmp[0]->name."] ";
					}
				?>
				$(".xrcbplayer .programa").text("<?php echo $programa; ?>");

				$(".xrcbplayer .podcast").html("<a href='<?php echo get_permalink(); ?>'><?php echo get_the_title(); ?></a>");
				$(".xrcbplayer .podcast-link").text("<?php echo get_permalink(); ?>");

				// load podcast info con parametros del player visible en ficha podcast
				$("#toggle-podcast").prop('checked', true);
				setTimeout(function(){ 
					clearInterval(pullTitleTimer);
					$(".xrcbplayer #live").removeClass("live");
					$(".broadcast-btn").removeClass("live");
					$(".xrcbplayer .sep").css("display", "inline-block");
					$(".xrcbplayer #live").text("");
					setPodcastInfo($(".btn-play")) }
				, 1000);

				<?php if (get_post_meta(get_the_ID(), 'live', true) == 'true') : ?>

					$(".xrcbplayer .player").addClass("src-stream");// set class playing stream
					$(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast

					$(".broadcast-btn > a").click();

				<?php else : ?>

					$(".xrcbplayer .player").removeClass("src-stream");// remove class playing stream
					$(".xrcbplayer .player").addClass("src-podcast");// set class playing podcast

				<?php endif; ?>
			}
		}
	});
</script>
