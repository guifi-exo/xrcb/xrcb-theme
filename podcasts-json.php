<?php
/**
	Template Name: Podcasts JSON
*/

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

/* all podcasts which are not live*/
$args = array(
	'post_type' => 'podcast',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'order' => 'ASC',
	'orderby' => 'ID',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key'     => 'live',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key'     => 'live',
			'value'   => 'true',
			'compare' => '!=',
		),
	),
);

if (isset($_GET['podcast_id'])) {
	$args['p'] = $_GET['podcast_id'];
}
else if (isset($_GET['radio_id'])) {
	$args['meta_key'] = 'radio';
	$args['meta_value'] = $_GET['radio_id'];
}
else if (isset($_GET['program_id'])) {
	$args['tax_query'] = array(
		array(
			'taxonomy' 	=> 'podcast_programa',
			'terms' 	=> $_GET['program_id'],
		),
	);
}

$my_query = new WP_Query($args);

if ( $my_query->have_posts() ) {

	$data = array();

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$location = get_post_meta(get_post_meta(get_the_ID(), 'radio', true), 'location', true);
		$address = "";
		$lat = 0;
		$lon = 0;
		
		if (!empty($location)) {
			$address = $location["address"];
			$lat = (float)$location["lat"];
			$lon = (float)$location["lng"];
		}

		$programa = get_the_terms( get_the_ID(), 'podcast_programa' );
		$programa_id = -1;
		if ($programa && count($programa) > 0) {
			$programa_id = $programa[0]->term_id;
			$programa = $programa[0]->name;
		} else {
			$programa = "";
		}

		if (get_post_meta(get_post_meta(get_the_ID(), 'radio', true), 'sede', true) !== "fantasma") {

			$data[] = array(
				"id" => (int)get_the_ID(),
				"title" => html_entity_decode(get_the_title()),
				"description" => html_entity_decode(get_the_excerpt()),
				"radio_id" => (int)get_post_meta(get_the_ID(), 'radio', true),
				"radio_name" => get_the_title(get_post_meta(get_the_ID(), 'radio', true)),
				"radio_permalink" => get_permalink(get_post_meta(get_the_ID(), 'radio', true)),
				"radio_lat" => $lat,
				"radio_lon" => $lon,
				"programa" => $programa,
				"programa_id" => (int)$programa_id,
				"fecha_emision" => get_post_meta(get_the_ID(), 'fecha_emision', true),
				"fecha_publicacion" => get_the_date( 'Ymd' ),
				"file_mp3" => wp_get_attachment_url(get_post_meta(get_the_ID(), 'file_mp3', true)),
				"file_mp3_meta" => wp_get_attachment_metadata(get_post_meta(get_the_ID(), 'file_mp3', true)),
				"permalink" => get_permalink(get_the_ID()),
			);
		}
	}

	if (sizeOf($data) == 1) $data = $data[0];

	echo json_encode(array("data" => $data));
}

fclose($fp);

?>