<?php
/**
	Template Name: Calendario JSON

	Gets all appointments and associated podcast data from DB
*/


include '/var/www/html/wordpresspreprod/wp-load.php';


$start = $_GET['start'];
$end = $_GET['end'];

//$start = '2023-11-15';
//$end = '2023-11-15';

// Short-circuit if the client did not give us a date range.
if (!isset($start) || !isset($end)) {
  die("Please provide a date range.");
}

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

$params = array();

// filter events from date range
$query_programacio = $wpdb->prepare("
SELECT 
    a.id, 
    a.date, 
    a.start, 
    a.end_date, 
    a.end, 
    GROUP_CONCAT(
        CONCAT('{\"field_id\":', f.field_id, ', \"value\":\"', f.value, '\"}')
        SEPARATOR ','
    ) AS aggregated_fields
FROM wp_ea_appointments AS a
LEFT JOIN wp_ea_fields AS f ON a.id = f.app_id AND f.field_id IN (5, 6, 10, 11)
WHERE a.date>='".$start."' and a.date<='".$end."'
	GROUP BY 
    a.id, 
    a.date, 
    a.start, 
    a.end_date, 
    a.end
ORDER BY a.date, a.start;",
	$params
);
$programacio = $wpdb->get_results($query_programacio);

$calendario = [];

$i = 0;
foreach ($programacio as $prog) {

	//print_r($prog);

	$aggregated_fields = json_decode('['.$prog->aggregated_fields.']');

	//print_r($aggregated_fields);

	$fields = array(
		'title' => '',
		'podcast_id' => null,
		'empty' => false,
		'algorithm' => false
	);

	foreach($aggregated_fields as $field) {
		switch($field->field_id) {
			case 5:
				$fields['podcast_id'] = $field->value;
				break;
			case 6:
				$fields['algorithm'] = $field->value == 1;
				break;
			case 10:
				if ($field->value != "")
					$fields['empty'] = true;
				break;
			case 11:
				$fields['title'] = $field->value;
				break;
		}
	}

	//trigger_error(json_encode($fields), E_USER_WARNING);

	$calendario[$i] = new stdClass();
	$calendario[$i]->start = $programacio[$i]->date . "T" . $programacio[$i]->start;
	$calendario[$i]->end = $programacio[$i]->end_date . "T" . $programacio[$i]->end;
	$calendario[$i]->empty = $fields['empty'];

	if ($fields['podcast_id']) {
		// PODCAST

		$podcast_id = $fields['podcast_id'];

		$calendario[$i]->title = get_post($podcast_id)->post_title;
		$calendario[$i]->live = get_post_meta($podcast_id, 'live', true)=="true";
		$calendario[$i]->file = wp_get_attachment_url(get_post_meta($podcast_id, 'file_mp3', true));
		$calendario[$i]->radio = get_post(get_post_meta($podcast_id, 'radio', true))->post_title;
		$calendario[$i]->radiolink = get_the_permalink(get_post_meta($podcast_id, 'radio', true));
		$calendario[$i]->podcastlink = get_post_permalink($podcast_id);
		$calendario[$i]->algorithm = $fields['algorithm'];
	}
	else {
		// LIVE

		$calendario[$i]->title = $fields['title'];
		$calendario[$i]->live = true;

		$calendario[$i]->file = false;
		$calendario[$i]->radio = false;
		$calendario[$i]->radiolink = false;
		$calendario[$i]->podcastlink = false;
		$calendario[$i]->algorithm = false;
	}

	$i++;
}

echo json_encode($calendario);

fclose($fp);

?>
