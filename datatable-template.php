<?php
/**
 * Template Name: Datatable
 *
 * @package xrcb
 */

get_header(); ?>

	<div id="primary" class="content-area" style="width:980px;">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<div class="entry-content">

				<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

				<div id="datatable"></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="radio-table"></table>');
		$('#radio-table').dataTable({
			paging: false,
			responsible: true,
			search: true,
			stateSave: true,
			ajax: { url :"<?php echo get_site_url(); ?>/index.php/radios-json/", type : "GET"},
			columns: [
				{ "data": "title", "title" : "Nom", "render": function ( data, type, row ) { return "<a href='"+row["permalink"]+"'>"+data+"</a>"; }, },
				{ "data": "categoria", "title" : "Categoria"},
				{ "data": "sede", "title" : "Sede"},
				{ "data": "barrio", "title" : "Barri"},
				{ "data": "address", "title" : "Direcció"},
				{ "data": "year", "title" : "Año fundación"},
				{ "data": "licencia", "title" : "Licencia"},
				{ "data": "web", "title" : "URL", "render": function ( data, type, row ) { return "<a href='"+data+"'>"+data+"</a>"; }, },
			]
		});
	});
</script>

<?php get_footer(); ?>
