<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package xrcb
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content category-escolaxrcb" role="main">
		<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
				<?php
					printf( __( '%s', 'xrcb' ), single_cat_title( '', false ) );
				?>
				</h1>
				<div class="btn-cat">
					<!--<a class="btn-cat" href="<?php //echo esc_url( home_url( '/' ) ); ?>category/escolaxrcb/" title="totes">totes</a>-->

					<?php
						// show all sub categories of category "escolaxrcb"
						$args = array('child_of' => 1721, 'hide_empty' => 0);
						$categories = get_categories( $args );
						foreach($categories as $category) { 
						    echo '<a class="btn-cat disable" href="#" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
						}
					?>
				</div>

			</header><!-- .page-header -->

			<div class="intro">
				<?php echo category_description( 1721 ); ?> 
			</div>

			<div class="articles">

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', 'escolaxrcb' );
					?>

				<?php endwhile; ?>

				<?php xrcb_content_nav( 'nav-below' ); ?>

			</div>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>
