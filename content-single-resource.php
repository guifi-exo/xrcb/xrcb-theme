<?php
/**
 * @package xrcb
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php //xrcb_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
			$attachments = get_posts( array(
	            'post_type' => 'attachment',
	            'posts_per_page' => -1,
	            'post_parent' => $post->ID,
	            'exclude'     => get_post_thumbnail_id()
	        ) );
	 
	        if ( $attachments ) {
	            foreach ( $attachments as $attachment ) {
	                if ( $attachment->post_mime_type == 'audio/mpeg' ) : ?>

						<div class='btn btn-play piwik_download' data-src='<?php echo wp_get_attachment_url( $attachment->ID ); ?>' data-radio='<?php echo get_the_author(); ?>' data-title='<?php echo wp_get_attachment_metadata( $attachment->ID )["title"]; ?>' data-radio-link='<?php echo get_permalink(); ?>' data-podcast-link='<?php echo get_permalink(); ?>'></div>

					<?php 
						echo wp_get_attachment_metadata( $attachment->ID )["title"];
						echo " <a class='download piwik_download' href='" . wp_get_attachment_url( $attachment->ID ) . "' download>[download]</a><br />";

					endif;
	            }
	            echo "<br />";
	        }
        ?>

		<?php 
			if ( has_post_thumbnail() ) the_post_thumbnail('thumbnail', array('class' => 'alignright'));

			the_content();
		?>

		<?php if (!has_excerpt()): ?>
			<div><br />Artist: <strong><!--<a href='<?php //echo get_site_url(); ?>/<?php //echo wpm_get_language(); ?>/index.php/author/<?php //echo get_the_author(); ?>'>--><?php echo get_the_author(); ?><!--</a>--></strong></div>
			<div><?php echo nl2br(get_the_author_meta('description')); ?></div>
		<?php endif; ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'xrcb' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'xrcb' ) );

			if ( ! xrcb_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'xrcb' );
				}

			} // end check for categories on this blog

			/*printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);*/
		?>

		<?php edit_post_link( __( 'Edit', 'xrcb' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->