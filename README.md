xrcb: Template für Wordpress
==============================

`xrcb` is a theme for Wordpress. It's based on `_s`, or [`underscores`](http://underscores.me/).

The theme is ajax enabled and thought for mapping projects. It uses leaflet library to show locations on a OpenStreetMap and intends to stay always with the same map as background.

## Custom post type `radio`

All registers of this post type are drawn by the template on the map. The only fields which have to be defined are `latitude` and `longitude`. The post type can easily be extended adding fields `radio` post type at `functions.php` and modifying template `content-single-radio.php`.

## Page templates

- Home: Hides content window, typically used for home pages which directly want to show a map.
- Export JSON: Exports all registers of post type `radio` to Json.
- Export GeoJSON: Exports all registers of post type `radio` to GeoJson using latitude and longitude.
- DataTables: Shows all registers of post type `radio` as a [data table](http://www.datatables.net/).

## Install

1. Install wordpress theme as usual.
2. Create content for custom post type `radio`.
3. Create page /radios-geojson using page template `Export GeoJSON` (used by leaflet to generate markers on the background map).
4. Create page /radios-json using page template `Export JSON` (used by datatable).
5. Adapt menu.

More detailed instructions coming soon.

## Wordpress plugin dependencies

[Advanced Custom Fields v5 free](https://www.advancedcustomfields.com/) is necesarry for editing content of type `radio`. If you want to avoid this plugin, you have to implement a custom form for editing content of type `radio`. 

## Used JavaScript libraries

- [jQuery hashchange event v1.3](http://benalman.com/projects/jquery-hashchange-plugin/)
- [Leaflet 1.3.1](http://leafletjs.com/)
- [Leaflet.Locate 0.62.0](https://github.com/domoritz/leaflet-locatecontrol)
- [DataTables 1.10.16](http://www.datatables.net/)
- [MediaElements.js 4.2.9](http://www.mediaelementjs.com/)

## License

`xrcb` is free software and uses a [GPL license](LICENSE).
