<?php
/**
 * Template Name: Calendar fill day
 *
 * @package xrcb
 */

//if (!is_user_logged_in())
//	wp_redirect(home_url());
?>

<?php

// 0. load all podcasts
$podcasts = new WP_Query(
	array(
		'post_type' => 'podcast',
		'post_status' => 'publish',
		'posts_per_page' => '48',	// 48 slots of 30 minutes each
		'order' => 'ASC',
		'orderby' => 'rand'
	)
);
$podcasts = $podcasts->posts;
// print_r($podcasts);

// 1. read appointments for tomorrow
$query_programacio = $wpdb->prepare("SELECT id,date,start,end
	FROM wp_ea_appointments
	WHERE date = CURDATE() + INTERVAL 1 DAY
	ORDER BY date ASC,start",
	array()
);
$programacio = $wpdb->get_results($query_programacio);
//print_r($programacio);

// 2. write appointments to temporary calendar array
$calendar = [];
foreach ($programacio as $prog) {
	$start = new DateTime($prog->start);
	$end = new DateTime($prog->end);

	// how many slots of 30 minutes for this program?
	$diff = $end->diff($start, true);
	$slots = ($diff->h*60+$diff->i)/30;

	for ($i=0; $i<$slots; $i++) {
		$starttime = strtotime($prog->start)+$i*30*60;
		$startdatetime = date("H:i:s",$starttime);
		$calendar[$startdatetime] = $prog->id;
	}
}
//print_r($calendar);

// 3. iterate over calendar and fill empty slots with random podcasts
$slots = 24*60/30;
$num = 0;
for ($i=0; $i<$slots; $i++) {
	$starttime = date("H:i:s", $i*30*60);
	//echo "starttime:".$starttime."\n";
	//echo "cal:".isset($calendar[$starttime])."---".$calendar[$starttime]."\n";

	if(!isset($calendar[$starttime])) {
		// slot empty -> get random podcast
		var_dump($starttime);

		$podcast_id = array_pop($podcasts)->ID;

		if ($podcast_id !== NULL) {

			// get podcast mp3 file
			$live = get_post_meta($podcast_id, 'live', true);
			$mp3 = wp_get_attachment_metadata(get_post_meta($podcast_id, 'file_mp3', true));

			// only if not live podcast and has mp3 file
			if (!$live && !empty($mp3) && isset($mp3['length_formatted'])) {

				// get podcast duration
				$duration = $mp3['length_formatted'];
				//var_dump($podcast_id, $duration, strlen($duration));

				if (!empty($duration) && $duration !== '' && $duration !== "0") {
					if (strlen($duration) < 7) $duration = "0:".$duration;
					
					// exact end time
					/*$endtime = date("H:i:s", strtotime($starttime) + strtotime($duration));
					var_dump($starttime, $duration, $endtime);*/

					// full slot end time
					$newslots = ceil((date("H", strtotime($duration))*60+date("i", strtotime($duration)))/30);
					$endtime = date("H:i:s", strtotime($starttime)+$newslots*30*60);
					//var_dump($starttime, $duration, $newslots, $endtime);

					// write to database
					$query_newprogramacio = $wpdb->prepare("INSERT INTO wp_ea_appointments (`location`, `service`, `worker`, `date`, `start`, `end`, `end_date`, `status`, `price`)
						VALUES (1, 1, 1, CURDATE() + INTERVAL 1 DAY, '%s', '%s', CURDATE() + INTERVAL 1 DAY, 'confirmed' ,0.0)",
						$starttime,
						$endtime
					);
					$wpdb->get_results($query_newprogramacio);
					$programacio_id = $wpdb->insert_id;

					// write podcast_id to database
					$query_podcastlink = $wpdb->prepare("INSERT INTO wp_ea_fields (`app_id`, `field_id`, `value`)
						VALUES (%d, 5, '%s')",
						$programacio_id,
						$podcast_id
					);
					$wpdb->get_results($query_podcastlink);

					$query_podcastalgo = $wpdb->prepare("INSERT INTO wp_ea_fields (`app_id`, `field_id`, `value`)
						VALUES (%d, 6, TRUE)",
						$programacio_id
					);
					$wpdb->get_results($query_podcastalgo);

					// avoid next slots already occupied by this programacio
					$i = $i+$newslots-1;

					$num++;
				}
			}
		}
	}
}

echo $num." podcasts add to calendar\n";

?>