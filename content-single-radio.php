<?php

/**
 * @package xrcb
 */

?>

<?php
function getRadioDescription($id)
{

	if (wpm_get_language() == 'es') $lang = '_es';
	else if (wpm_get_language() == 'en') $lang = '_en';
	else $lang = '';

	$desc = '';
	$txt = get_field('historia' . $lang);

	if ($txt && strlen($txt) > 200) {
		//$desc .= '<div class="resume">' . rtrim(substr($txt,0,200)) . '...</div>';
		$desc .= '<div class="total">' . $txt . '</div>';
		$desc .= '<div class="more-button"><i class="fa fa-plus" aria-hidden="true"></i></div>';
		$desc .= '<script>
				jQuery(document).ready(function($) {
					$(".more-button").click(function() {
						//$(".description .resume").toggle();
						$(".description .total").toggle();
						$(".more-button .fa").toggleClass("fa-plus");
						$(".more-button .fa").toggleClass("fa-minus");
					});
				});
			</script>';
	} else {
		$desc .= $txt;
	}

	return $desc;
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php $radio = get_the_title(); ?>

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<?php
		// limit live player funcionality to Radio Rambles and Radio Fabra
		if (get_the_ID() == 5919 || get_the_ID() == 864) :
			// limit live player funcionality to Radio Guifinet preprod
			//if (get_the_ID() == 4188) :

			date_default_timezone_set("Europe/Madrid");
			//echo date("H:i:s");

			$params = array();

			$query_programacio = $wpdb->prepare(
				"SELECT id,start,end
					FROM wp_ea_appointments
					WHERE date = CURDATE() AND start < CURTIME() AND end > CURTIME()",
				$params
			);
			$programacio_tmp = $wpdb->get_results($query_programacio);

			//print_r($programacio_tmp);
			//echo count($programacio_tmp);

			if (count($programacio_tmp) > 0) :
				$programacio_id = $programacio_tmp[0]->id;

				$query_live = $wpdb->prepare(
					"SELECT id
						FROM wp_ea_fields
						WHERE app_id=$programacio_id AND field_id=13 AND value=1",
					$params
				);
				//print_r($wpdb->get_results($query_live));
				if (count($wpdb->get_results($query_live)) > 0) :
		?>
					<aside class="live-player">
						<div class="btn-play-container">
							<div class="btn btn-play btn-play-inverse piwik_download" data-src="https://icecast.xrcb.cat/main.mp3" data-radio="Ràdio Rambles" data-title="Ràdio Rambles Live" data-radio-link="https://xrcb.cat/ca/radio/radiorambles/" data-podcast-link="https://xrcb.cat/ca/podcast/radio-rambles-live/" data-programa=""></div>
						</div>
						<span> LIVE <?php echo $programacio_tmp[0]->start . "-" . $programacio_tmp[0]->end ?> h.</span>
					</aside>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>

		<div class="entry-meta">
			<?php if (has_post_thumbnail()) the_post_thumbnail('full'); ?>
			<?php //xrcb_posted_on(); 
			?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">

		<div class="entry-content-radio">
			<?php
			$radio_id = $post->ID;
			the_content();
			$location = get_post_meta($post->ID, 'location', true);
			?>
			<!-- radio extra info-->
			<aside class="radio-extra-info">

				<div class="radio-category">
					radio <?php echo get_post_meta($post->ID, 'categoria', true); ?>
				</div>

				<div class="radio-location">
					<a class="btn-radio" href="#" data-lat='<?php if (!empty($location)) echo $location["lat"]; ?>' data-lon='<?php if (!empty($location)) echo $location["lng"]; ?>'>
						<i class="fas fa-map-marker-alt"></i>
						<span class="radio-coordinates">
							lat: <?php if (!empty($location)) echo $location["lat"]; ?>
							lon: <?php if (!empty($location)) echo $location["lng"]; ?>
						</span>
						<span class="radio-location-name">
							<?php echo get_post_meta($post->ID, 'barrio', true); ?>
						</span>
					</a>
				</div>

				<div class="radio-year-started">
					<?php _e(get_post_meta($post->ID, 'anyo_fundacion', true), 'xrcb'); ?>
				</div>

				<div class="radio-license-type">
					<?php
					$license = get_post_meta($post->ID, 'licencia', true);
					if ($license == "CC") {
						echo '<i class="fab fa-creative-commons" aria-hidden="true"></i>';
					} else if ($license == "CR") {
						echo '<i class="far fa-copyright" aria-hidden="true"></i>';
					}
					?>
				</div>

				<div class="radio-website-link">
					<a target="_blank" href="<?php _e(get_post_meta($post->ID, 'web', true), 'xrcb'); ?>"><span class="radio-url"><?php _e(get_post_meta($post->ID, 'web', true), 'xrcb'); ?></span></a>
				</div>

				<div class="radio-frequency">
					<?php $fm = get_post_meta($post->ID, 'fm', true);
					echo (empty($fm) ? '' : 'FM ' . $fm); ?>
				</div>

			</aside>
			<!-- radio call -->
			<div class="radio-call">
				<?php
				if (wpm_get_language() == 'es') $lang = '_es';
				else if (wpm_get_language() == 'en') $lang = '_en';
				else $lang = '';

				$convocatoria = get_post_meta($post->ID, 'convocatoria' . $lang, true);

				if (!empty($convocatoria)) {
					echo ' / <a class="nounderline bold" target="_blank" href="' . $convocatoria . '">';

					$convocatoria_title = get_post_meta($post->ID, 'convocatoriatitle' . $lang, true);

					if (!empty($convocatoria_title)) {
						echo $convocatoria_title;
					} else {
						if (wpm_get_language() == 'ca') echo 'convocatòria ' . date("Y");
						if (wpm_get_language() == 'es') echo 'convocatoria ' . date("Y");
						if (wpm_get_language() == 'en') echo 'call ' . date("Y");
					}

					echo '</a>';
				}
				?>
			</div>
			<!-- radio description -->
			<div class="description radio-description">
				<?php echo getRadioDescription($post->ID); ?>
			</div>

			<?php
			// get podcasts
			$podcast_query = array(
				'posts_per_page' => '-1',
				'post_type' => 'podcast',
				//'orderby' => 'post_date',
				//'order' => 'DESC',
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key'     => 'radio',
						'value'   => $post->ID,
						'compare' => '=',
					),
					array(
						'relation' => 'OR',
						array(
							'key'     => 'live',
							'compare' => 'NOT EXISTS',
						),
						array(
							'key'     => 'live',
							'value'   => 'true',
							'compare' => '!=',
						),
					),
				),
			);
			$podcast_posts = new WP_Query($podcast_query);

			// get reposts
			$repost_query = array(
				'posts_per_page' => '-1',
				'post_type' => 'repost',
				'author'   => get_the_author_meta('ID'),
				//'orderby' => 'post_date',
				//'order' => 'DESC',
			);
			$repost_posts = new WP_Query($repost_query);

			// merge podcasts and reposts
			$result_podcast_repost = new WP_Query();
			$result_podcast_repost->posts = array_merge($podcast_posts->posts, $repost_posts->posts);
			$result_podcast_repost->posts = wp_list_sort($result_podcast_repost->posts, 'post_date', 'DESC');
			$result_podcast_repost->post_count = count($result_podcast_repost->posts);

			// get programs
			$programes = get_terms('podcast_programa', array(
				'orderby'    => 'date',
			));
			$progs = [];
			foreach ($programes as $programa) {
				$user = get_field('usuari', 'podcast_programa_' . $programa->term_id);
				if ($user['ID'] == get_the_author_meta('ID')) {
					$progs[$programa->term_id] = $programa->name;
				}
			}

			if (count($progs) > 0) : ?>

				<div class="entry-content-programes">
			  	<!--div class="more-button more-button-programes"><i class="fa fa-plus" aria-hidden="true"></i></div-->
			  	<script>
			  	//	jQuery(document).ready(function($) {
			  	//		$(".more-button-programes").click(function() {
			  	//			//$(".description .resume").toggle();
			  	//			$(".description .entry-content-programes").toggle();
			  	//			$(".more-button .fa").toggleClass("fa-plus");
			  	//			$(".more-button .fa").toggleClass("fa-minus");
			  	//		});
			  	//	});
			  	</script>
					<link rel="stylesheet" href="/wp-content/themes/xrcb/vendor/prettydropdowns.css">
					<script src="/wp-content/themes/xrcb/vendor/jquery.prettydropdowns.js"></script>
					<h3>programes (total <?php echo count($progs); ?>)</h3>
					<a target="_blank" id="btn-subscribe-program" class="btn-cat" href="/subscribirse-a-podcast/?id=<?php echo $post->ID; ?>">SUBSCRIBE TO PROGRAM</a>
					<a style="display:none" target="_blank" id="btn-subscribe-program_new" class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/wp-json/xrcb/v1/podcasts?radio_id=<?php echo $post->ID; ?>&format=application/rss+xml">NEW API</a>

					<select id="programes">
						<option value="all">totes (<?php echo $podcast_posts->found_posts; ?>)</option>
						<?php foreach ($progs as $key => $label) {
							$query = new WP_Query(
								array(
									'posts_per_page' => '-1',
									'post_type' => 'podcast',
									'meta_query' => array(
										array(
											'key'     => 'radio',
											'value'   => $post->ID,
											'compare' => '=',
										),
										array(
											'relation' => 'OR',
											array(
												'key'     => 'live',
												'compare' => 'NOT EXISTS',
											),
											array(
												'key'     => 'live',
												'value'   => 'true',
												'compare' => '!=',
											),
										),
									),
									'tax_query' => array(
										array(
											'taxonomy' => 'podcast_programa',
											'field'    => 'name',
											'terms'    => $label,
										),
									),
								)
							);
							echo '<option value="' . $key . '">' . $label . ' (' . $query->found_posts . ')</option>';
						} ?>
					</select>

				<?php endif; ?>

				</div>

				<div class="entry-content-podcasts">

					<h3>podcasts (total <?php echo $result_podcast_repost->post_count; ?>)</h3>
					<a target="_blank" id="btn-subscribe-radio" class="btn-cat" href="/subscribirse-a-podcast/?id=<?php echo $post->ID; ?>">SUBSCRIBE TO RADIO</a>
					<a style="display:none" target="_blank" class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/wp-json/xrcb/v1/podcasts?radio_id=<?php echo $post->ID; ?>&format=application/rss+xml">NEW API</a>

					<table class="radio-podcasts">

						<?php

						$i = 0;
						while ($result_podcast_repost->have_posts()) : $result_podcast_repost->the_post();

							$terms = get_the_terms(get_the_ID(), 'podcast_programa');

							//if ($i == 1) print_r(get_field('podcast'));
							if (get_post_type(get_the_ID()) == 'podcast') {
								$id = get_the_ID();
								$permalink = get_the_permalink();
							} else {
								$id = get_field('podcast')->ID;
								$permalink = get_post_permalink($id);
							}

							$length = wp_get_attachment_metadata(get_post_meta($id, 'file_mp3', true))['length_formatted'];
							$mp3_url = wp_get_attachment_url(get_post_meta($id, 'file_mp3', true));
							$radio_link = get_the_permalink(get_post_meta($id, 'radio', true));
						?>

							<tr <?php if ($i > 2) echo " class='more hidden " . get_post_type(get_the_ID()) . "'"; ?> class='<?php echo get_post_type(get_the_ID()); ?>' data-program="<?php echo $terms[0]->term_id; ?>">
								<td class="publish_date">
									<?php
									$date = get_post_meta(get_the_ID(), 'fecha_emision', true);
									if ($date != "") {
										echo substr($date, 6) . '/' . substr($date, 4, 2) . '/' . substr($date, 2, 2);
									} else {
										echo get_the_date('d/m/y');
									}
									?>
								</td>
								<td class="name"><a href="<?php echo $permalink; ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a> <?php if (get_field('podcast')) echo '&nbsp;&nbsp;<i class="fas fa-retweet" title="' . get_field('radio', get_field('podcast')->ID)->post_title . '"></i>'; ?></td>
								<td class="length"><?php echo $length; ?></td>
								<td class="mp3">
									<div class='btn btn-play piwik_download' data-src='<?php echo $mp3_url ?>' data-radio='<?php echo $radio; ?>' data-title='<?php the_title(); ?>' data-link='<?php echo $permalink; ?>' data-radio-link='<?php echo $radio_link ?>' data-podcast-link='<?php echo $permalink; ?>'></div>
								</td>
							</tr>

						<?php
							$i++;
						endwhile;
						?>

					</table>

					<?php if ($i > 2) : ?>
						<div class='more-btn'><i class='fa fa-plus' aria-hidden='true'></i>
							<script>
								jQuery(document).ready(function($) {
									$('.more-btn .fa').click(function() {
										$('.more').toggleClass('hidden');
										$('.more-btn .fa').toggleClass('fa-plus');
										$('.more-btn .fa').toggleClass('fa-minus');
									});
								});
							</script>
						</div>
					<?php endif; ?>
				</div>
		</div> <!-- end entry content radio -->

	</div>

	<footer class="entry-meta">
		<?php edit_post_link(__('Edit', 'xrcb'), '<span class="edit-link">', '</span>', $radio_id); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->

<script type="text/javascript">
	var $programSelect = null;

	jQuery(document).ready(function($) {
		bindPlayer();

		$("a.btn-radio").click(function() {
			if ($(this).data("lat") && $(this).data("lon")) {
				globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
			}
		});

		// convert select in prettydropdown
		if ($('#programes').length > 0) {
			$programSelect = $('#programes').prettyDropdown({
				classic: true,
				customClass: 'arrow triangle small',
				height: 30
			});

			$("#prettydropdown-programes").click(function() {
				setProgramFilter();
			});
		}

		function setProgramFilter() {
			var value = $("#prettydropdown-programes").find("li.selected").data("value");
			//console.log("setProgramFilter", value);

			if (value === undefined) {
				value = "all";
				$("#programes").val(value);
				if ($programSelect) $programSelect.refresh();
			}

			if (value == "all") {
				$(".radio-podcasts tr").addClass("more hidden");
				$(".radio-podcasts tr:nth-child(-n+3)").removeClass("more hidden");

				// write url
				var url = location.href;
				if (location.href.indexOf("?program") !== -1) {
					var url = location.href.substring(0, location.href.indexOf("?program"));
				}
				window.history.pushState("object or string", "Title", url);

				// hide program subscription button
				$("#btn-subscribe-program").removeClass("active");
			} else {
				$(".radio-podcasts tr").addClass("hidden");
				$(".radio-podcasts tr").removeClass("more");
				$(".radio-podcasts tr[data-program='" + value + "']").removeClass("hidden");
				$(".radio-podcasts tr[data-program='" + value + "']").addClass("more");
				$(".radio-podcasts tr[data-program='" + value + "']:nth-child(-n+3)").removeClass("more");

				// write url
				var url = location.href;
				if (location.href.indexOf("?program") !== -1) {
					var url = location.href.substring(0, location.href.indexOf("?program"));
				}
				url += "?program=" + value;
				window.history.pushState("object or string", "Title", url);

				// show program subscription button
				$("#btn-subscribe-program").attr("href", $("#btn-subscribe-program").attr("href") + "&id_program=" + value);
				$("#btn-subscribe-program_new").attr("href", $("#btn-subscribe-program_new").attr("href") + "&program_id=" + value);
				$("#btn-subscribe-program").addClass("active");
			}

			//console.log($(".radio-podcasts tr.more.hidden").length);

			if ($(".radio-podcasts tr.more.hidden").length > 0) {
				$('.more-btn').removeClass("hidden");
				$('.more-btn i').addClass("fa-plus");
				$('.more-btn i').removeClass("fa-minus");
			} else {
				$('.more-btn').addClass("hidden");
			}
		}

		// preload program filter
		let params = new URLSearchParams(document.location.search.substring(1));
		let program = params.get("program");

		if (program === undefined || program === null) program = "all";

		if (program !== null) {
			//console.log("set program by url", program);
			$("#programes").val(program);
			if ($programSelect) $programSelect.refresh();
			setProgramFilter();

			// bind click event
			$("#prettydropdown-programes").click(function() {
				setProgramFilter();
			});
		}
	});
</script>
