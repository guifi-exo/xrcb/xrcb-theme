/*
	based on Tutorial "How To AJAXify WordPress Theme"
	https://deluxeblogtips.com/how-to-ajaxify-wordpress-theme/
*/

jQuery(document).ready(function($) {

	// add class for active menu item
	/*$('#menu-principal a').click(function() {
		$('#menu-principal li').removeClass('current_page_item');
		$(this).parent().addClass('current_page_item');
	});*/

    var $mainContent = $("#primary"),
    siteUrl = window.location.protocol.toString() + "//" + window.location.host.toString(),
    url = '';

	$(document).on("click", "a[href^='"+siteUrl+"']:not([href*='/wp-admin/']):not([href*='/wp-login.php']):not([href$='/feed/']):not([href*='/wp-content/uploads/']):not([href*='/wp-content/plugins/']):not([href='/ca/']):not([href='/es/']):not([href='/en/']):not([href*='/publish-podcast/'])", function(e) {
		//console.log(e.currentTarget.search, this.pathname);

		var lang = document.documentElement.lang;
		var hrefes = this.pathname.replace('/'+lang+'/', '/es/');
		$(".wpm-language-switcher .item-language-es a").attr("href", hrefes);
		var hrefca = this.pathname.replace('/'+lang+'/', '/ca/');
		$(".wpm-language-switcher .item-language-ca a").attr("href", hrefca);
		var hrefen = this.pathname.replace('/'+lang+'/', '/en/');
		$(".wpm-language-switcher .item-language-en a").attr("href", hrefen);

		if (e.currentTarget.search)
			location.hash = this.pathname + e.currentTarget.search;
		else
			location.hash = this.pathname;

		return false;
	});

    $("#searchform").submit(function(e) {
		location.hash = '?s=' + $("#s").val();
		e.preventDefault();
    });

    $(window).bind('hashchange', function(){

    	url = window.location.hash.substring(1);

		//exclude URLs from ajax behavior
		if (!url ||
			url.indexOf("comment")!= -1 ||
			url.indexOf("respond")!= -1 ||
			url.indexOf("json")!= -1) {
			return;
		}

		url = url + " #content";

		// deactivate carousel
		$('#primary').removeClass("carousel");

		$mainContent.animate({opacity: "0.1"}).html('<img style="padding:20px;" src="'+window.location.protocol+"//"+window.location.host+'/wp-content/themes/xrcb/images/ajax-loader.gif" />').load(url, function(data) {
			$mainContent.animate({opacity: "1"});

			var oPageInfo = {
	            title: data.substring(data.indexOf("<title>")+7,data.indexOf("</title>")),
	            url: window.location.origin+window.location.hash.substring(1)
	        }
		    //console.log(window.history.state, oPageInfo);

		    window.history.replaceState(oPageInfo, oPageInfo.title, oPageInfo.url);
		    //console.log(window.history.state);

			//datatable radios
			if (url.indexOf("llistat-radios")!= -1) {
				//load js
				$.getScript(siteUrl + "/wp-content/themes/xrcb/vendor/jquery.dataTables.min.js")
					.done(function(script, textStatus) {
						//apply data table
						$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="radio-table"></table>');
						var datatable = $('#radio-table').DataTable({
							paging: false,
							info: false,
							responsible: true,
							search: true,
							stateSave: true,
							ajax: { url :"/"+document.documentElement.lang+"/radios-json/", type : "GET"},
							columns: [
								{ "data": "title", "title" : "Nom", "className" : "name", "render": function ( data, type, row ) { return "<a href='"+row["permalink"]+"' onclick='globalmap.setView(new L.LatLng("+row["lat"]+","+row["lon"]+"), 18, false)'>"+data+"</a>"; }, },
								{ "data": "categoria", "title" : "Categoria", "className" : "category"},
								{ "data": "barrio", "title" : "Barri", "className" : "neighbourhood"},
								{ "data": "year", "title" : "Año fundación", "className" : "year"},
								{ "data": "licencia", "title" : "Licencia", "className" : "license"},
								{ "data": "web", "title" : "URL", "className" : "url", "render": function ( data, type, row ) { return "<a target='_blank' href='"+data+"'>"+data+"</a>"; }, },
							]
						});
						datatable.on( 'init.dt', function () {
							// show radios count
							$("#radio-table_wrapper").prepend("<div class='dataTables_length radios-count'><span class='recordsDisplay'>"+datatable.page.info()["recordsDisplay"]+"</span> / "+datatable.page.info()["recordsTotal"]+" Radios</div>");

						    $("a.btn-radio").click(function(){
						        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
						    });
						});
						datatable.on( 'search.dt', function () {
							// modificar podcast count
							$("#radio-table_wrapper .recordsDisplay").text(datatable.page.info()["recordsDisplay"]);
						});
					})
					.fail(function(jqxhr, settings, exception) {
					console.log("Triggered ajaxError handler. ",jqxhr,settings,exception );
				});
				$('#primary').addClass('llistat-radios');
			}

			//single radio or podcast
			else if (url.indexOf("/radio/")!= -1 ||
				url.indexOf("/podcast/")!= -1 ||
				url.indexOf("/resource/")!= -1 ||
				url.indexOf("/author/")!= -1) {

				bindPlayer();

				$(".more-button").click(function() {
					$(".description .resume").toggle();
					$(".description .total").toggle();
					$(".more-button .fa").toggleClass("fa-plus");
					$(".more-button .fa").toggleClass("fa-minus");
				});

			    $(".podcast .btn-embed").click(function(){
			    	$(".podcast .embed_code").toggle();
			    });

			    $(".podcast .btn-twitter").click(function(){
					createTwitterMsg($(".btn-radio").text(), $(".podcast .entry-title").text(), document.location.href);
			    });

				$('#primary').addClass('radio');
			}

			//datatable podcasts
			else if (url.indexOf("llistat-podcasts")!= -1) {
				//load js
				$.getScript(siteUrl + "/wp-content/themes/xrcb/vendor/jquery.dataTables.min.js")
					.done(function(script, textStatus) {
						//apply data table
						$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="podcast-table"></table>');
						var datatable = $('#podcast-table').DataTable({
							paging: false,
							info: false,
							order: [[ 4, "desc" ]],
							responsible: true,
							search: true,
							stateSave: true,
							ajax: { url :"/"+document.documentElement.lang+"/podcasts-json/", type : "GET"},
							columns: [
								{ "data": "title", "title" : "Nom", "className" : "name", "render": function ( data, type, row ) { return "<a href='"+row["permalink"]+"'>"+data+"</a>"; }, },
								{ "data": "description", "title" : "Descripció", "className" : "description"},
								{ "data": "radio_name", "title" : "Radio", "className" : "radio", "render": function ( data, type, row ) { return "<a href='"+row["radio_permalink"]+"'>"+data+"</a>"; }, },
								{ "data": "programa", "title" : "Programa", "className" : "program", "render": function ( data, type, row ) { return "<a class='btn-programa' href='"+row["radio_permalink"]+"?program="+row["programa_id"]+"'>"+data+"</a>"; }, },
								{ "data": "fecha_publicacion", "title" : "Data publicació", "className" : "publish_date", "render": function ( data, type, row ) {
										// if display or filter data is requested, format the date
										if ( type === 'display' || type === 'filter' ) {
											if (data != "") {
								            	return data.substring(6)+"/"+data.substring(4,6)+"/"+data.substring(0,4);
											} else {
												return "";
											}
										}
										else {
											return data;
										}
									},
								},
								{ "data": "file_mp3", "title" : "MP3", "className" : "mp3", "render": function ( data, type, row ) { return "<div class='btn btn-play piwik_download' data-src='"+data+"' data-radio='"+row['radio_name']+"' data-title='"+row['title']+"' data-radio-link='"+row["radio_permalink"]+"' data-podcast-link='"+row["permalink"]+"' data-programa='"+row["programa"]+"'></div>"; }, },
							]
						});
						datatable.on( 'init.dt', function () {
							bindPlayer();

							// show podcast count
							$("#podcast-table_wrapper").prepend("<div class='dataTables_length podcast-count'><span class='recordsDisplay'>"+datatable.page.info()["recordsDisplay"]+"</span> / "+datatable.page.info()["recordsTotal"]+" Podcasts</div>");

						    $("a.btn-radio").click(function(){
						        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
						    });
						});
						datatable.on( 'search.dt', function () {
							// modificar podcast count
							$("#podcast-table_wrapper .recordsDisplay").text(datatable.page.info()["recordsDisplay"]);
						});

					})
					.fail(function(jqxhr, settings, exception) {
					console.log("Triggered ajaxError handler. ",jqxhr,settings,exception );
				});

				if (window.screen.availWidth > 1024) {
				}
				$('#primary').addClass('llistat-podcasts');
			}

			//datatable resources
			else if (url.indexOf("recursos")!= -1) {
				//load js
				$.getScript(siteUrl + "/wp-content/themes/xrcb/vendor/jquery.dataTables.min.js")
					.done(function(script, textStatus) {
						//apply data table
						$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="resources-table"></table>');
						$('#resources-table').DataTable({
							paging: false,
							info: false,
							responsible: true,
							search: true,
							stateSave: true,
							ajax: { url :"/"+document.documentElement.lang+"/resources-json/", type : "GET"},
							columns: [
								{ "data": "title", "title" : "Nom i enllaç", "render": function ( data, type, row ) { if (row["url"] !== "") return "<a target='_blank'  href='"+row["url"]+"'>"+data+"</a>"; else return "<a href='"+row["permalink"]+"'>"+data+"</a>"; }, },
								{ "data": "description", "title" : "Descripció", "render": function ( data, type, row ) { if (row["excerpt"] !== "") return row["excerpt"]; else return data; },},
								{ "data": "author", "title" : "Autor"/*, "render": function ( data, type, row ) { return "<a href='/author/"+data+"'>"+data+"</a>"; }*/ },
								{ "data": "files_mp3", "title" : "MP3s", "render": function ( data, type, row ) { var files=""; data.forEach(function(file) { files += "<span class='btn btn-play piwik_download' data-src='"+file.url+"' data-radio='"+row['author']+"' data-title='"+file.title+"' data-radio-link='"+row["permalink"]+"' data-podcast-link='"+row["permalink"]+"'></span>"+file.title+"<br />"; }); return files; } },
								/*{ "data": "url", "title" : "URL", "render": function ( data, type, row ) { var url=siteUrl + "/"; if (data.startsWith(url)) { var link=document.documentElement.lang+"/"+data.substring(url.length); return "<a href='"+link+"'>https://xrcb.cat</a>"; } else return "<a target='_blank' href='"+data+"'>"+data+"</a>"; },},*/
							],
						}).on( 'init.dt', function () {
							bindPlayer();
						});

					})
					.fail(function(jqxhr, settings, exception) {
					console.log("Triggered ajaxError handler. ",jqxhr,settings,exception );
				});

				if (window.screen.availWidth > 1024) {
				}
				$('#primary').addClass('recursos');
			}

			//fullcalendar
			else if (url.indexOf("calendari")!= -1) {
				//load js
				$.when(
				    $.getScript( siteUrl + "/wp-content/themes/xrcb/vendor/moment.min.js" ),
				    $.getScript( siteUrl + "/wp-content/themes/xrcb/vendor/fullcalendar.min.js" ),
				    $.Deferred(function( deferred ){
				        $( deferred.resolve );
				    })
				).done(function(){
					$.getScript( siteUrl + "/wp-content/themes/xrcb/vendor/fullcalendar-locale-all.js" )
					.done(function(script, textStatus) {

						//apply css
						$('#script-warning').hide();

						//apply fullcalendar
					    $('#calendar').fullCalendar({
							header: {
								left: 'prev,next today',
								center: 'title',
								right: 'month,agendaWeek,listDay'
							},
							views: {
								listDay: {
									type: 'listDay',
									duration: { days: 1 },
									buttonText: 'Dia'
								}
							},
							//initialView: 'listDay',
							locale: document.documentElement.lang,
							timeFormat: 'HH:mm',
							navLinks: true, // can click day/week names to navigate views
							eventLimit: 4,
							viewRender: function(view, element) {
								// add handler to watch items inserted into .fc-view to catch popovers and re-init player
								$('.fc-view').on('DOMNodeInserted', function (e) {
									if ($(e.target).hasClass('fc-popover')) {
										bindPlayer();
									}
								});
							},
							events: {
								url: siteUrl + '/wp-content/themes/xrcb/inc/get-events.php',
								error: function() {
									$('#script-warning').show();
								}
							},
							eventRender: function(event, element) {

								if (!event.empty) {

									if (!event.live) {
										//calendar view
										$(".fc-content", element).prepend("<div class='btn btn-play piwik_download' data-src='"+event.file+"' data-radio='"+event.radio+"' data-title='"+event.title+"' data-radio-link='"+event.radiolink+"' data-podcast-link='"+event.podcastlink+"'></div>");
										// list view
										$(".fc-list-item-title", element).prepend("<div class='btn btn-play piwik_download' data-src='"+event.file+"' data-radio='"+event.radio+"' data-title='"+event.title+"' data-radio-link='"+event.radiolink+"' data-podcast-link='"+event.podcastlink+"'></div>");

										// add class if event was created by algorithm and not by human
										if (event.algorithm) {
											$(".fc-content", element).addClass("algorithm");
											$(".fc-list-item-title", element).parent().addClass("algorithm");
										}
									}
									else {
										// add class if event has flag LIVE
										$(".fc-content", element).addClass("live");
										$(".fc-list-item-title", element).parent().addClass("live");

										// show end time when LIVE event
										if (event.end) {
											let time = event.end._i;
											time = time.substr(time.indexOf("T")+1,5);
											$(".fc-time", element).text($(".fc-time", element).text()+"-"+time);
										}
									}
								} else {
									$(".fc-content", element).addClass("empty");
								}
							},
							eventAfterAllRender: function() {
								bindPlayer();
							},
					    	loading: function(bool) {
					        	$('#loading').toggle(bool);
					    	}
					    });

					    // by default show agenda 
						$('#calendar').fullCalendar('changeView', 'listDay');
					});
				});

				if (window.screen.availWidth > 1024) {
				}
				$('#primary').addClass('calendari');
			}

			// apply css
			else if (url.indexOf("manual-de-usuario")!= -1 ||
					url.indexOf("manual-de-usuario-2")!= -1 ||
					url.indexOf("manual-de-usuario-3")!= -1) {
			}

			else if (url.indexOf("programacio-de-emissions") != -1 ||
				url.indexOf("category") != -1) {

				if (window.screen.availWidth > 1024) {
				}
				$('#primary').addClass('category');
			}

			else if (url === "/ca/ #content" || url === "/es/ #content" || url === "/en/ #content") {
				$.when(
				    $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/min/tiny-slider.js" ),
				    $.getScript( siteUrl + "/wp-content/plugins/xrcb-carousel/carousel.js" ),
				    $.Deferred(function( deferred ){
				        $( deferred.resolve );
				    })
				).done(function(){
					$("#primary").removeClass("category").addClass("carousel");
				});
			}

			else {
				$('#primary').addClass('standard');
				$('p').addClass('standard');
			}

			if (url.indexOf("etiquetado-de-podcasts") != -1) {
				$('#primary').addClass('etiquetado-de-podcasts');
			}

			//pretty dropdown
			if (url.indexOf("/radio/")!= -1) {
				//load js
				$.when(
				    $.getScript( siteUrl + "/wp-content/themes/xrcb/vendor/jquery.prettydropdowns.js" ),
				    //$.getScript( siteUrl + "/wp-content/themes/xrcb/vendor/prettydropdowns.css" ),
				    $.Deferred(function( deferred ){
				        $( deferred.resolve );
				    })
				).done(function(){

					$("a.btn-radio").click(function(){
				        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
				    });

					// convert select in prettydropdown
					if ($('#programes').length > 0) {
					    $programSelect = $('#programes').prettyDropdown({
					    	classic: true,
						    customClass: 'arrow triangle small',
						    height: 30
						});

						$("#prettydropdown-programes").click(function() {
							setProgramFilter();
						});
					}

					function setProgramFilter() {
						var value = $("#prettydropdown-programes").find("li.selected").data("value");
						//console.log("setProgramFilter", value);

						if (value == "all") {
							$(".radio-podcasts tr").addClass("more hidden");
							$(".radio-podcasts tr:nth-child(-n+3)").removeClass("more hidden");

							// write url
							var url = location.href;
							if (location.href.indexOf("?program") !== -1) {
								var url = location.href.substring(0,location.href.indexOf("?program"));
							}
							window.history.pushState("object or string", "Title", url);

							// hide program subscription button
							$("#btn-subscribe-program").removeClass("active");
						}
						else {
							$(".radio-podcasts tr").addClass("hidden");
							$(".radio-podcasts tr").removeClass("more");
							$(".radio-podcasts tr[data-program='"+value+"']").removeClass("hidden");
							$(".radio-podcasts tr[data-program='"+value+"']").addClass("more");
							$(".radio-podcasts tr[data-program='"+value+"']:nth-child(-n+3)").removeClass("more");

							// write url
							var url = location.href;
							if (location.href.indexOf("?program") !== -1) {
								var url = location.href.substring(0,location.href.indexOf("?program"));
							}
							url += "?program="+value;
							window.history.pushState("object or string", "Title", url);

							// show program subscription button
							$("#btn-subscribe-program").attr("href", $("#btn-subscribe-program").attr("href")+"&id_program="+value);
							$("#btn-subscribe-program").addClass("active");
						}

						//console.log($(".radio-podcasts tr.more.hidden").length);

						if ($(".radio-podcasts tr.more.hidden").length > 0) {
							$('.more-btn').removeClass("hidden");
							$('.more-btn i').addClass("fa-plus");
							$('.more-btn i').removeClass("fa-minus");
						}
						else {
							$('.more-btn').addClass("hidden");
						}
					}
					// preload program filter
					let params = new URLSearchParams(document.location.search.substring(1));
					let program = params.get("program");
					if (program !== null) {
						//console.log("set program by url", program);
						$("#programes").val(program);
						$programSelect.refresh();
						setProgramFilter();

						// bind click event
						$("#prettydropdown-programes").click(function() {
							setProgramFilter();
						});
					}

					$('.more-btn .fa').click(function() {
						$('.more').toggleClass('hidden');
						$('.more-btn .fa').toggleClass('fa-plus');
						$('.more-btn .fa').toggleClass('fa-minus');
					});
				});
			}

			bindPlayer();

		    // remove bold from active menu
			$(".close-button").click(function() {
				console.log("close ajax");
				$('#menu-principal li').removeClass('current_page_item');
			});

		    // go back in history
			$(".back-button").click(function() {
				window.history.back();
			});
		});
    });
    $(window).trigger('hashchange');

    // remove bold from active menu
	$(".close-button").click(function() {
		console.log("close ajax");
		$('#menu-principal li').removeClass('current_page_item');
	});

    // go back in history
	$(".back-button").click(function() {
		window.history.back();
	});

    /* redirect to calendar when new programació */
    document.addEventListener( 'easyappnewappointment', function( event ) {
    	//console.log("nou programació", event);

    	window.location = siteUrl + "/ca/calendari/";

	}, false );
});

window.onpopstate = function(event) {

	if (event.state) {
		var host = "https://"+location.hostname;
		var newHash = event.state.url.substring(host.length);

		//console.log(event.state, location.hash, newHash);

		// !!! without next line location history seems to be right
		location.hash = newHash;
	}

	//console.log(window.history.length);
};

window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};
