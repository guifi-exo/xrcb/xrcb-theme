jQuery(document).ready(function($) {
    if (window.mobilecheck()) {
        loadBG();
        checkMobileLiveRedirect();
    }
    else {
        loadMap();
        checkDesktopLiveRedirect();
    }
    bindButtons();

    function checkMobileLiveRedirect() {
        var liveUrl = checkLiveUrl();
        if (!liveUrl) {
            // $("#mobileplayer").css("background-image", "url(https://xrcb.cat/wp-content/uploads/2018/07/gof-01_transp.gif)");
            $("#mobileplayer").addClass("xrcb-mobileview-big-player");
        }
        else {
            // $("#mobileplayer").css("background-image", "url(https://xrcb.cat/wp-content/uploads/2019/11/Marca-City-Sound-CAS.jpg)");
            $("#mobileplayer").addClass("xrcb-mobileview-big-player-alt");
            setTimeout(function(){
                playBroadcast(true);
                //$("#mobileplayer .play").click();
            }, 500);
        }
    }

    function checkDesktopLiveRedirect() {
        var liveUrl = checkLiveUrl();
        if (liveUrl === "ca") {
            //window.location.href = "https://xrcb.cat/ca/event/12-11-citysound-a-smart-city-week/";
            window.location.replace("https://xrcb.cat/ca/event/12-11-citysound-a-smart-city-week/");
        }
        else if (liveUrl === "es") {
            //window.location.href = "https://xrcb.cat/ca/event/12-11-citysound-a-smart-city-week/";
            window.location.replace("https://xrcb.cat/es/event/12-11-citysound-a-smart-city-week/");
        }
        else if (liveUrl === "en") {
            //window.location.href = "https://xrcb.cat/ca/event/12-11-citysound-a-smart-city-week/";
            window.location.replace("https://xrcb.cat/es/event/12-11-citysound-a-smart-city-week/");
        }
    }

    function checkLiveUrl() {
        if (document.URL === "https://xrcb.cat/ca/citysound" ||
            document.URL === "https://xrcb.cat/ca/live") {
            return "ca";
        }
        else if (document.URL === "https://xrcb.cat/es/citysound" ||
            document.URL === "https://xrcb.cat/es/live") {
            return "es";
        }
        else if (document.URL === "https://xrcb.cat/en/citysound" ||
            document.URL === "https://xrcb.cat/en/live") {
            return "en";
        }
        else {
            return false;
        }
    }

    function loadBG() {
        var btn = '<button class="play" type="button" title="Reprodueix"></button>';
        $("#mobileplayer").append("<div class='mobileplayer'>"+btn+"</div>");

        $("#mobileplayer .play").click(function() {
            if (!$(this).hasClass("pause")) {
                $(".xrcbplayer .mejs-play button").click();
                player.play();
                $(this).addClass("pause");
            }
            else {
                player.pause();
                $(this).removeClass("pause");
            }
        });

        $(".share.more").attr("title", "emissió XRCB");
    }
});
