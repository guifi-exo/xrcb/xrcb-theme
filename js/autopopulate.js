jQuery(document).ready(function($) {

    $city = $('#acf-field_5a685b74baaa2');
    $barrio = $('#acf-field_5a685b74baee2');
    
    /*console.log("city", $city.find(":selected").length);
    console.log("barrio", $barrio.find(":selected").length);
    if ($city.find(":selected").length === 1) {
        console.log($city.find(":selected").val().trim());
        console.log($barrio.find(":selected").val().trim());
    }*/

    // Get city option on select menu change
    $city.change(function () {
        if ($city.find(":selected").length === 1) {
            //console.log($(this).find(":selected").val().trim());
            loadNeighbourhoods($(this).find(":selected").val().trim());
        }
    }).change();

    function loadNeighbourhoods(city) {
        // add options for selected city to select
        if (city === 'Barcelona' || city == 'Hospitalet de Llobregat' || city == 'Esplugues de Llobregat' || city == 'El Prat de Llobregat' || city == 'Altres') {
            //$('#acf-field_5a685b74baee2').empty();

            var data = [{id: "", text: ""}];
            barris[city].forEach(function(barrio) {
                //console.log(barrio);
                data.push({id: barrio, text: barrio});
                //$('#acf-field_5a685b74baee2').append("<option value='"+barrio+"'>"+barrio+"</option>");
            });

            if ($('#acf-field_5a685b74baee2').hasClass("select2-hidden-accessible")) {
                $('#acf-field_5a685b74baee2').select2('destroy').empty();
            }
            
            $('#acf-field_5a685b74baee2').select2({
                data: data,
                placeholder: "Selecciona un barrio"
            }).trigger('change');

            /*$('#acf-field_5a685b74baee2').find("option").each(function() {
                console.log($(this).text());
            });*/
        }
    }
});

var barris = {
    "Barcelona": [
        "el Raval",
        "el Barri Gòtic",
        "la Barceloneta",
        "Sant Pere, Santa Caterina i la Ribera",
        "el Fort Pienc",
        "la Sagrada Família",
        "la Dreta de l\'Eixample",
        "l\'Antiga Esquerra de l\'Eixample",
        "la Nova Esquerra de l\'Eixample",
        "Sant Antoni",
        "el Poble Sec",
        "la Marina del Prat Vermell",
        "la Marina de Port",
        "la Font de la Guatlla",
        "Hostafrancs",
        "la Bordeta",
        "Sants - Badal",
        "Sants",
        "les Corts",
        "la Maternitat i Sant Ramon",
        "Pedralbes",
        "Vallvidrera, el Tibidabo i les Planes",
        "Sarrià",
        "les Tres Torres",
        "Sant Gervasi - la Bonanova",
        "Sant Gervasi - Galvany",
        "el Putxet i el Farró",
        "Vallcarca i els Penitents",
        "el Coll",
        "la Salut",
        "la Vila de Gràcia",
        "el Camp d\'en Grassot i Gràcia Nova",
        "el Baix Guinardó",
        "Can Baró",
        "el Guinardó",
        "la Font d\'en Fargues",
        "el Carmel",
        "la Teixonera",
        "Sant Genís dels Agudells",
        "Montbau",
        "la Vall d\'Hebron",
        "la Clota",
        "Horta",
        "Vilapicina i la Torre Llobeta",
        "Porta",
        "el Turó de la Peira",
        "Can Peguera",
        "la Guineueta",
        "Canyelles",
        "les Roquetes",
        "Verdun",
        "la Prosperitat",
        "la Trinitat Nova",
        "Torre Baró",
        "Ciutat Meridiana",
        "Vallbona",
        "la Trinitat Vella",
        "Baró de Viver",
        "el Bon Pastor",
        "Sant Andreu",
        "la Sagrera",
        "el Congrés i els Indians",
        "Navas",
        "el Camp de l\'Arpa del Clot",
        "el Clot",
        "el Parc i la Llacuna del Poblenou",
        "la Vila Olímpica del Poblenou",
        "el Poblenou",
        "Diagonal Mar i el Front Marítim del Poblenou",
        "el Besòs i el Maresme",
        "Provenals del Poblenou",
        "Sant Martí de Provenals",
        "la Verneda i la Pau"
    ],
    "Hospitalet de Llobregat": [
        "El Centre",
        "Sant Josep",
        "Sanfeliu",
        "Collblanc",
        "La Torrassa",
        "Santa Eulalia",
        "Granvia Sur",
        "La Florida",
        "Les Planes",
        "Pubilla Casas",
        "Can Serra",
        "Bellvitge",
        "El Gornal",
        "Distrito Económico"
    ],
    "Esplugues de Llobregat": [
        "El Gall",
        "La Plana",
        "Montesa",
        "Can Vidalet",
        "El Centre",
        "Can Clota",
        "Ciutat Diagonal",
        "Finestrelles",
        "La Miranda",
        "La Mallola"
    ],
    "El Prat de Llobregat": [
        "El Prat de Llobregat"
    ],
    "Altres": [
        "Guinea Ecuatorial"
    ]
}