<?php
/**
 * Template Name: Podcasts Datatable
 *
 * @package xrcb
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="page-header">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<!--<div><a class="btn-cat" href="<?php //echo get_site_url(); ?>/<?php //echo wpm_get_language(); ?>/calendari/">Switch to calendar view</a></div>-->
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.js"></script>
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables-accent-neutralise.js"></script>

				<div id="datatable"></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="podcast-table"></table>');
		var datatable = $('#podcast-table').DataTable({
			paging: false,
			info: false,
			order: [[ 4, "desc" ]],
			responsible: true,
			search: true,
			stateSave: true,
			ajax: { url :"<?php echo get_site_url().'/'.wpm_get_language(); ?>/podcasts-json/", type : "GET"},
			columns: [
				{ "data": "title", "title" : "Nom", "className" : "name", "render": function ( data, type, row ) {
						return "<a href='"+row["permalink"]+"'>"+data+"</a>";
					},
				},
				{ "data": "description", "title" : "Descripció", "className" : "description"},
				{ "data": "radio_name", "title" : "Radio", "className" : "radio", "render": function ( data, type, row ) {
						return "<a class='btn-radio' href='"+row["radio_permalink"]+"' data-lat='"+row["radio_lat"]+"' data-lon='"+row["radio_lon"]+"'>"+data+"</a>";
					},
				},
				{ "data": "programa", "title" : "Programa", "className" : "program", "render": function ( data, type, row ) {
						return "<a class='btn-programa' href='"+row["radio_permalink"]+"?program="+row["programa_id"]+"'>"+data+"</a>";
					},
				},
				{ "data": "fecha_publicacion", "title" : "Data", "className" : "publish_date", "render": function ( data, type, row ) {
						// if display or filter data is requested, format the date
						if ( type === 'display' || type === 'filter' ) {
							if (data != "") {
				            	return data.substring(6)+"/"+data.substring(4,6)+"/"+data.substring(0,4);
							} else {
								return "";
							}
						}
						else {
							return data;
						}
					},
				},
				{ "data": "file_mp3", "title" : "MP3", "className" : "mp3", "render": function ( data, type, row ) {
						return "<div class='btn btn-play piwik_download' data-src='"+data+"' data-radio='"+row['radio_name']+"' data-title='"+row['title']+"' data-programa='"+row['programa']+"' data-radio-link='"+row["radio_permalink"]+"' data-podcast-link='"+row["permalink"]+"'></div>";
					},
				},
			],
		}).on( 'init.dt', function () {
			bindPlayer();

			// show podcast count
			$("#podcast-table_wrapper").prepend("<div class='dataTables_length podcast-count'><span class='recordsDisplay'>"+datatable.page.info()["recordsDisplay"]+"</span> / "+datatable.page.info()["recordsTotal"]+" Podcasts</div>");

		    $("a.btn-radio").click(function(){
		        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
		    });
		}).on( 'search.dt', function () {
			// modificar podcast count
			$("#podcast-table_wrapper .recordsDisplay").text(datatable.page.info()["recordsDisplay"]);
		});
	});
</script>

<?php get_footer(); ?>
