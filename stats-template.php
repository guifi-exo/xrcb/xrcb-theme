<?php
/**
	Template Name: Stats

    - radios más activas (podcasts)
    - programas más activos (podcasts)
    - etiquetas más populares
    - categorías más populares
*/

if (!is_user_logged_in())
	wp_redirect(home_url());

echo "<h1>XRCB Stats</h1>";

$radio_args = array(
    'post_type' => 'radio',
	'post_status' => 'publish',
	'posts_per_page' => '-1',
);
$radios = new WP_Query($radio_args);
echo "<p>total radios: <a href='".get_home_url()."/llistat-radios/'>" . $radios->found_posts . "</a><br>";

$radio_args = array(
    'post_type' => 'radio',
	'post_status' => 'publish',
	'posts_per_page' => '-1',
	'meta_query' => array(
		array(
			'key'     => 'sede',
			'value'   => 'fantasma',
		),
	),
);
$radios_fantasma = new WP_Query($radio_args);
echo "radios fantásmas: " . $radios_fantasma->found_posts . "</p>";

$podcast_args = array(
    'post_type' => 'podcast',
	'post_status' => 'publish',
	'posts_per_page' => '-1',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key'     => 'live',
			'compare' => 'NOT EXISTS',
		),
		array(
			'key'     => 'live',
			'value'   => 'true',
			'compare' => '!=',
		),
	),
);
$podcasts = new WP_Query($podcast_args);
echo "<p>total podcasts: <a href='".get_home_url()."/llistat-podcasts/'>" . $podcasts->found_posts . "</a><br>";

$podcast_args = array(
    'post_type' => 'podcast',
	'post_status' => 'publish',
	'posts_per_page' => '-1',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key'     => 'live',
			'value'   => 'true',
		),
		array(
			'key'     => 'live',
			'value'   => 'false',
			'compare' => '!=',
		),
	),
);
$podcasts = new WP_Query($podcast_args);
echo "podcasts live: " . $podcasts->found_posts . "</p>";

/********************/
echo "<h2>radios más activas</h2>";

class Radio {
    public $count;
    public $link;
}
$radios_sort = array();

if ( $radios->have_posts() ) {

	while ($radios->have_posts()) {

		$radios->the_post();

		$radio_podcast_args = array(
		    'post_type' => 'podcast',
			'post_status' => 'publish',
		    'meta_key'     => 'radio',
			'meta_value'   => get_the_ID(),
		);
		$radio_podcasts = new WP_Query($radio_podcast_args);
		$radio = new Radio();
		$radio->count = $radio_podcasts->found_posts;
		$radio->link = "<a href='" . get_permalink() . "'>" . get_the_title() . "</a>";
		$radios_sort[] = $radio;
	}
}

echo "<ol>";
usort($radios_sort, function($a, $b) { return $a->count < $b->count; });
foreach($radios_sort as $count => $radio) {
	echo "<li>" . $radio->link . ": " . $radio->count . " </li>";
}
echo "</ol>";

/********************/
echo "<h2>programas más activos</h2>";

class Programa {
    public $count;
    public $link;
}
$programas_sort = array();

$programas = get_terms(array('podcast_programa'));

foreach ($programas as $programa) {
	$programa_args = array(
	    'post_type' => 'podcast',
		'tax_query' => array(
			array(
				'taxonomy' => 'podcast_programa',
				'field'    => 'slug',
				'terms'    => $programa->slug,
			),
		),
	);
	$programas = new WP_Query($programa_args);

	if ( $programas->have_posts() ) {
		$programas->the_post();

		$radio = get_post_meta(get_the_ID(), 'radio', true);
		$prog = new Programa();
		$prog->count = $programas->found_posts;
		$prog->link = "<a href='" . get_the_permalink($radio) . "?program=" . $programa->term_id . "'>" . $programa->name . " - " . get_the_title($radio) . "</a>";
		$programas_sort[] = $prog;
	}
}

echo "<ol>";
usort($programas_sort, function($a, $b) { return $a->count < $b->count; });
foreach($programas_sort as $count => $programa) {
	echo "<li>" . $programa->link . ": " . $programa->count . " </li>";
}
echo "</ol>";


/********************/
echo "<h2>etiquetas más populares</h2>";

$tags_args = array(
    'taxonomy' => 'podcast_tag',
	'orderby' => 'count',
	'order' => 'DESC',
);
$tags = get_terms($tags_args);

echo "<ol>";
foreach ($tags as $tag) {
	if ($tag->count > 1) {
		echo "<li>" . $tag->name . ": " . $tag->count . " </li>";
	}
}
echo "</ol>";

/********************/
echo "<h2>categorías más populares</h2>";

$categorys_args = array(
    'taxonomy' => 'podcast_category',
	'orderby' => 'count',
	'order' => 'DESC',
);
$categorys = get_terms($categorys_args);

echo "<ol>";
foreach ($categorys as $category) {
	echo "<li>" . $category->name . ": " . $category->count . " </li>";
}
echo "</ol>";

?>
