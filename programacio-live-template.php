<?php
/**
 * Template Name: Programacio Live
 *
 * @package xrcb
 */

// only for admins
if (!is_user_logged_in() && !current_user_can('manage_options'))
	wp_redirect(home_url());

get_header(); ?>

	<div id="primary" class="content-area" style="width:980px;">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<div class="entry-content booking-live">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<?php
					echo do_shortcode( '[ea_bootstrap width="960px" scroll_off="true" layout_cols="2"]' );

					// set live input
					echo "<script>jQuery(document).ready(function($) {jQuery('#live').val('1')});</script>";
				?>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
