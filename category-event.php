<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package xrcb
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content category-event" role="main">
		<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

		<header class="page-header">
			<h1 class="page-title">
				<?php
					printf( __( '%s', 'xrcb' ), single_cat_title( '', false ) );
				?>
			</h1>
			<?php if (wpm_get_language() == 'ca'): ?>
				<!--<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/ca/category/event/" title="View all events">totes</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/ca/category/event-next/" title="View all future events">propers</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/ca/category/event-past/" title="View all past events">passats</a>-->
			<?php elseif (wpm_get_language() == 'es'): ?>
				<!--<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/es/category/event/" title="View all events">todos</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/es/category/event-next/" title="View all future events">próximos</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/es/category/event-past/" title="View all past events">pasados</a>-->
			<?php elseif (wpm_get_language() == 'en'): ?>
				<!--<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/en/category/event/" title="View all events">all</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/en/category/event-next/" title="View all future events">next</a>
				<a class="btn-cat" href="<?php echo get_stylesheet_directory_uri(); ?>/en/category/event-past/" title="View all past events">past</a>-->
			<?php endif; ?>

		</header><!-- .page-header -->

		<?php /* Start the Loop */ ?>
		<?php
			$today = getdate();
			$args = array(
				'post_type' => 'post',
				'category_name' => 'event',
				'post_status' => array('future', 'publish'),
				'posts_per_page' => 30,
				'order_by' => 'date',
				'order'   => 'DESC',
			);
			$query = new WP_Query( $args );
		?>

		<div class="articles">

			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'event' );
				?>

			<?php endwhile; ?>

			<?php xrcb_content_nav( 'nav-below' ); ?>

		</div>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>
