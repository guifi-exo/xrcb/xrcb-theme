<?php
/**
 * Template Name: Programacio
 *
 * @package xrcb
 */

if (!is_user_logged_in())
	wp_redirect(home_url());

get_header(); ?>

	<div id="primary" class="content-area" style="width:980px;">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<div class="entry-content booking">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<?php

					echo do_shortcode( '[ea_bootstrap width="960px" scroll_off="true" layout_cols="2"]' );

					$author = "";
					if (!current_user_can('administrator')) {
						$author = "&author=".get_current_user_id();
					}
					//echo is_admin().$author;

					$my_query = new WP_Query('post_type=podcast&post_status=publish&posts_per_page=500&order=DESC&orderby=ID'.$author);

					if ( $my_query->have_posts() ) {

						$data = array();

						while ($my_query->have_posts()) {

							$my_query->the_post();

							//echo get_the_title()." [".get_the_ID()."]";

							$duration = "";
							$mp3 = wp_get_attachment_metadata(get_post_meta(get_the_ID(), 'file_mp3', true));
							if (!empty($mp3) && isset($mp3["length_formatted"])) {
								$duration = "[".$mp3["length_formatted"]."]";
							}
							echo "<script>jQuery(document).ready(function($) {";
							echo "$('select.form-control').append($('<option>', {    value: ".get_the_ID().", text: '".get_the_title()." (".get_the_author_meta('user_nicename').") ".$duration."'}));";
							echo "});</script>";

						}
					}

				?>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
