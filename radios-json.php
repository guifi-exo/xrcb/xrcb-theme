<?php
/**
	Template Name: Radios JSON
*/

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

$my_query = new WP_Query('post_type=radio&post_status=publish&posts_per_page=100&order=ASC&orderby=ID');

if ( $my_query->have_posts() ) {

	$data = array();

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$location = get_post_meta(get_the_ID(), 'location', true);
		$address = "";
		$lat = 0;
		$lon = 0;

		if (!empty($location)) {
			$address = $location["address"];
			$lat = (float)$location["lat"];
			$lon = (float)$location["lng"];
		}

		if (get_post_meta(get_the_ID(), 'sede', true) !== "fantasma") {

			$data[] = array(
				"id" => (int)get_the_ID(),
				"title" => get_the_title(),
				"categoria" => get_the_terms( get_the_ID(), 'radio_category' )[0]->name,
				"sede" => get_post_meta(get_the_ID(), 'sede', true),
				"barrio" => get_post_meta(get_the_ID(), 'barrio', true),
				"address" => $address,
				"year" => get_post_meta(get_the_ID(), 'anyo_fundacion', true),
				"licencia" => get_post_meta(get_the_ID(), 'licencia', true),
				"web" => get_post_meta(get_the_ID(), 'web', true),
				"permalink" => get_permalink(get_the_ID()),
				"lat" => $lat,
				"lon" => $lon
			);
		}
	}

	echo json_encode(array("data" => $data));
}

fclose($fp);

?>