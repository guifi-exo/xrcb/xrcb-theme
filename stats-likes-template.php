<?php
/**
 * Template Name: podcast likes statistics
 *
 * @package xrcb
 */

if (!is_user_logged_in())
	wp_redirect(home_url());

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="page-header">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<div class="entry-content">

				<h2>most liked podcasts</h2>

				<?php if (function_exists('get_most_rated')): ?>
				    <ol>
				        <?php get_most_rated('both', 1, 30); ?>
				    </ol>
				<?php endif; ?>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
